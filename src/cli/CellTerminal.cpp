#include <iostream>
#include "CellTerminal.h"

void CellTerminal::displayState(int cellID) const
{
    std::cout << "Cell #" << cellID << "\t   " << isChickenExist();
    std::cout << "\t  " << seedsCount() << "\t " << isEggExist();
    std::cout << '\n';
}

void CellTerminal::displayActions(int cellID) const
{
    if(this->isChickenExist())
    {
        std::cout << "Chicken from " << cellID << " have eaten 1 seeds!\n";
    }
    else
    {
        std::cout << "Chicken from " << cellID << " cell is dead!\n";
    }
}

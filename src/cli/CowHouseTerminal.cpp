#include <iostream>
#include "CowHouseTerminal.h"

void CowHouseTerminal::displayState(int numberCowHouse) const
{
    std::cout << "Cow  #" << numberCowHouse << "\t   " << isCowExist();
    std::cout << "\t  " << hayStacksCount() << "\t " << isMilkExist();
    std::cout << '\n';
}

void CowHouseTerminal::displayActions() const
{
    if(this->isCowExist())
    {
        std::cout << "Cow  have eaten 1 hay!\n";
    }
    else
    {
        std::cout << "Cow cowHouse is dead!\n";
    }
}


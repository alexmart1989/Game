#ifndef CELLTERMINAL_H
#define CELLTERMINAL_H
#include "./src/core/Cell.h"

class CellTerminal: public Cell
{
public:
    void displayState(int cellID) const;
    void displayActions(int cellID) const;
};

#endif // CELLTERMINAL_H

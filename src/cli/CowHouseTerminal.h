#ifndef COWHOUSETERMINAL_H
#define COWHOUSETERMINAL_H
#include "./src/core/CowHouse.h"

class CowHouseTerminal: public CowHouse
{
public:
    void displayState(int CowHouseID) const;
    void displayActions() const;
};

#endif // COWHOUSETERMINAL_H

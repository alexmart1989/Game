#ifndef ACTIONSMAINMENU_H
#define ACTIONSMAINMENU_H

enum ActionsMainMenu
{
    START,
    SAVE,
    LOAD,
    EXIT,
};

#endif // ACTIONSMAINMENU_H

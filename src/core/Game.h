#ifndef GAME_H
#define GAME_H
#include "cli/CellTerminal.h"
#include "CellsVector.h"
#include "cli/CowHouseTerminal.h"

class Game {
public:
    Game();
    Game(int cellCount);
    void start();
    void save(int fd) const;
    void load(int fd);
private:
    CellsVector<CellTerminal> m_cellsVector;
    CowHouseTerminal m_cowHouse;
    unsigned int m_grabbedEggs;
    unsigned int m_grabbedMilk;

    void showGameMenu() const;
    void showInfo() const;

    void gameActions(bool &shouldExit, int choice);
    bool checkGameOver() const;
    int getChoice() const;
    int getNumberSelectedItem() const;

    void actionsAboveCell();
    void actionsAboveCowHouse();
};

#endif

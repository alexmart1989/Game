#ifndef CELL_H
#define CELL_H

class Cell {
public:
    Cell();
    Cell(bool chickenExist, int seedsCount, bool eggExist);
    Cell(const Cell& temp) = default;
    int seedsCount() const;
    void addSeeds();
    bool isEggExist() const;
    bool isChickenExist() const;
    bool grabEgg();
    void tick();
    void saveToFile(int fd) const;
    void loadFromFile(int fd);
private:
    unsigned int m_seedsCount;
    bool m_eggExist;
    bool m_chickenExist;
};

#endif

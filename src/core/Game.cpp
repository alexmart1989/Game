#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include "Game.h"
#include "Actions.h"
#define COUNT_COW 1

Game::Game():
    m_cellsVector(),
    m_grabbedEggs(0),
    m_grabbedMilk(0)
{}

Game::Game(int cellCount):
    m_cellsVector(cellCount),
    m_grabbedEggs(0),
    m_grabbedMilk(0)
{}

int Game::getChoice() const
{
    int choice = 0;
    std::cin >> choice;
    return choice;
}

void Game::start()
{
    showInfo();
    bool shouldExit = false;
    while(!shouldExit)
    {
        showGameMenu();
        int choice = getChoice();
        gameActions(shouldExit, choice);

        if (!shouldExit)
        {
            showInfo();
            actionsAboveCell();
            actionsAboveCowHouse();
        }

        if (checkGameOver())
        {
            std::cout << "Game over!\n";
            break;
        }
    }
}

void Game::save(int fd) const
{
    this->m_cellsVector.saveToFile(fd);
    this->m_cowHouse.saveToFile(fd);
    write(fd, &this->m_grabbedEggs, sizeof(this->m_grabbedEggs));
    write(fd, &this->m_grabbedMilk, sizeof(this->m_grabbedMilk));
}

void Game::load(int fd)
{
    this->m_cellsVector.loadFromFile(fd);
    this->m_cowHouse.loadFromFile(fd);

    read(fd, &this->m_grabbedEggs, sizeof(this->m_grabbedEggs));
    read(fd, &this->m_grabbedMilk, sizeof(this->m_grabbedMilk));
}

void Game::showGameMenu() const
{
    std::cout << "\n****Game Menu****\n\n";
    std::cout << BACK << " = BACK\n";
    std::cout << ADD_SEED << " = ADD_SEED\n";
    std::cout << GRAB_EGG << " = GRAB_EGG\n";
    std::cout << ADD_HAY << " = ADD_HAY\n";
    std::cout << GRAB_MILK << " = GRAB_MILK\n";
    std::cout << DO_NOTHING << " = DO_NOTHING\n";
    std::cout << ADD_CELL << " = ADD_CELL\n";
    std::cout << '\n';
}

void Game::showInfo() const
{
    std::cout << '\n';
    std::cout << "\tANIMAL\tFOOD\tPRODUCT\n";

    for(int cellID = 0; cellID < this->m_cellsVector.size(); ++cellID)
    {
        this->m_cellsVector.cell(cellID)->displayState(cellID);
    }
    int cowHouseID = this->m_cellsVector.size();
    this->m_cowHouse.displayState(cowHouseID);

    std::cout << "\nGrabbed eggs: " << this->m_grabbedEggs;
    std::cout << "\nGrabbed Milk: " << this->m_grabbedMilk << '\n';
}

void Game::gameActions(bool &shouldExit, int choice)
{
    switch(choice)
    {
        case BACK:
        {
            shouldExit = true;
            break;
        }
        case ADD_SEED:
        {
            int selectedItem = getNumberSelectedItem();
            std::cout << "You select ADD_SEED to " << selectedItem << " cell\n";
            this->m_cellsVector.cell(selectedItem)->addSeeds();
            break;
        }
        case GRAB_EGG:
        {
            int selectedItem = getNumberSelectedItem();
            bool isGrabbedEgg = false;
            isGrabbedEgg = this->m_cellsVector.cell(selectedItem)->grabEgg();
            if(isGrabbedEgg)
            {
                std::cout << "You grab egg from " << selectedItem << " cell\n";
                this->m_grabbedEggs++;
            }
            else
            {
                std::cout << "Cell is empty. Try to next move!\n";
            }
            break;
        }
        case ADD_HAY:
        {
            std::cout << "You select ADD_HAY to cowHouse\n";
            this->m_cowHouse.addHayStack();
            break;
        }
        case GRAB_MILK:
        {
            bool isGrabbedMilk = false;
            isGrabbedMilk = this->m_cowHouse.grabMilk();
            if(isGrabbedMilk)
            {
                std::cout << "You grab milk from cowHouse\n";
                this->m_grabbedMilk++;
            }
            else
            {
                std::cout << "CowHouse is empty. Try to next move!\n";
            }
            break;
        }
        case DO_NOTHING:
        {
            std::cout << "Day off and you did nothing\n";
            break;
        }
        case ADD_CELL:
        {
            std::cout << "You add new cell\n";
            this->m_cellsVector.addCell();
            break;
        }
        default:
        {
            std::cout << "Wrong choice. Repeat\n";
            break;
        }
    }
}

bool Game::checkGameOver() const
{
    bool isGameOver = true;
    isGameOver = !this->m_cowHouse.isCowExist();

    for(int cellID = 0; cellID < this->m_cellsVector.size(); ++cellID)
    {
        if(this->m_cellsVector.cell(cellID)->isChickenExist())
        {
            isGameOver = false;
        }
    }
    return isGameOver;
}

int Game::getNumberSelectedItem() const
{
    int selectedItem = 0;
    bool isSelectedItemCorrect = false;
    while(!isSelectedItemCorrect)
    {
        int cellsAmount = this->m_cellsVector.size();
        std::cout << "Select cells 0.." << (cellsAmount - 1) << ": ";
        std::cin >> selectedItem;
        if(selectedItem >= 0 && selectedItem < cellsAmount)
        {
           isSelectedItemCorrect = true;
           break;
        }
        std::cout << "Wrong input! Repeat, please\n";
    }
    std::cout << '\n';

    return selectedItem;
}

void Game::actionsAboveCell()
{
    for(int cellID = 0; cellID < this->m_cellsVector.size(); ++cellID)
    {
        this->m_cellsVector.cell(cellID)->tick();
        this->m_cellsVector.cell(cellID)->displayActions(cellID);
    }
}

void Game::actionsAboveCowHouse()
{
    this->m_cowHouse.tick();
    this->m_cowHouse.displayActions();
}

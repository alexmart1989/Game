#ifndef APPLICATION_H
#define APPLICATION_H
#include <iostream>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include "Game.h"
#include "ActionsMainMenu.h"
#define CELL_COUNT 3
#define BUFFER_MAX 1024

class Application
{
public:
    Application();
    void run();
private:
    int openFile() const;
    void save(int fd) const;
    static Game load(int fd);
    void showMainMenu() const;
    int getChoice() const;
    void setFileName(char* filename) const;

    Game m_game;
};

#endif // APPLICATION_H

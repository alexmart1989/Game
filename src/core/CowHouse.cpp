#include <unistd.h>
#include "CowHouse.h"
#define INITIAL_COUNT_HAY 3
#define HAY_COUNT_TO_ADD 3
#define MAX_DAYS_WITHOUT_MILKED 3

CowHouse::CowHouse():
    m_cowExist(true),
    m_hayStackCount(INITIAL_COUNT_HAY),
    m_milkExist(false),
    m_daysNotMilk(0)
{}

CowHouse::CowHouse(bool cowExist, int hayStacksCount, bool milkExist):
    m_cowExist(cowExist),
    m_hayStackCount(hayStacksCount),
    m_milkExist(milkExist),
    m_daysNotMilk(0)
{}

CowHouse::CowHouse(bool cowExist, int hayStacksCount, bool milkExist, int daysNotMilked):
    m_cowExist(cowExist),
    m_hayStackCount(hayStacksCount),
    m_milkExist(milkExist),
    m_daysNotMilk(daysNotMilked)
{}

int CowHouse::hayStacksCount() const {
    return this->m_hayStackCount;
}

void CowHouse::addHayStack() {
    this->m_hayStackCount += HAY_COUNT_TO_ADD;
}

bool CowHouse::isCowExist() const {
    return this->m_cowExist;
}

bool CowHouse::isMilkExist() const {
    return this->m_milkExist;
}

bool CowHouse::grabMilk() {
    bool isMilked = this->m_milkExist;
    this->m_milkExist = false;
    this->m_daysNotMilk = 0;
    return isMilked;
}

int CowHouse::getDaysNotMilked() const {
    return this->m_daysNotMilk;
}

void CowHouse::tick() {
    if(this->m_cowExist)
    {
        this->m_milkExist = true;

        if(this->m_hayStackCount > 0 && this->m_daysNotMilk < MAX_DAYS_WITHOUT_MILKED)
        {
            this->m_hayStackCount--;
            this->m_daysNotMilk++;
        }
        else
        {
            this->m_cowExist = false;
            this->m_milkExist = false;
        }
    }
}

void CowHouse::saveToFile(int fd) const
{
    char cowExist = this->m_cowExist;
    write(fd, &cowExist, sizeof(cowExist));

    write(fd, &this->m_hayStackCount, sizeof(this->m_hayStackCount));

    char milkExist = this->m_milkExist;
    write(fd, &milkExist, sizeof(milkExist));

    write(fd, &this->m_daysNotMilk, sizeof(this->m_daysNotMilk));
}

void CowHouse::loadFromFile(int fd)
{
    char cowExist;
    read(fd, &cowExist, sizeof(cowExist));
    this->m_cowExist = cowExist;

    read(fd, &this->m_hayStackCount, sizeof(this->m_hayStackCount));

    char milkExist;
    read(fd, &milkExist, sizeof(milkExist));
    this->m_milkExist = milkExist;

    read(fd, &this->m_daysNotMilk, sizeof(this->m_daysNotMilk));
}


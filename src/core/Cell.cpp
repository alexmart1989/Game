#include <unistd.h>
#include "Cell.h"
#define INITIAL_COUNT_SEEDS 3
#define SEED_COUNT_TO_ADD 3


Cell::Cell()
{
    this->m_eggExist = false;
    this->m_chickenExist = true;
    this->m_seedsCount = INITIAL_COUNT_SEEDS;
}

Cell::Cell(bool chickenExist, int seedsCount, bool eggExist)
{
    this->m_chickenExist = chickenExist;
    this->m_seedsCount = seedsCount;
    this->m_eggExist = eggExist;
}

int Cell::seedsCount() const
{
    return m_seedsCount;
}

void Cell::addSeeds()
{
    this->m_seedsCount += SEED_COUNT_TO_ADD;
}

bool Cell::isEggExist() const
{
    return this->m_eggExist;
}

bool Cell::isChickenExist() const
{
    return this->m_chickenExist;
}

bool Cell::grabEgg()
{
    bool isGrabbed = this->m_eggExist;
    this->m_eggExist = false;
    return isGrabbed;
}

void Cell::tick()
{
    if (this->m_chickenExist)
    {
        this->m_eggExist = true;

        if(this->m_seedsCount > 0)
        {
            this->m_seedsCount--;
        }
        else
        {
            this->m_chickenExist = false;
        }
    }
}

void Cell::saveToFile(int fd) const
{
    char chickenExist = this->m_chickenExist;
    write(fd, &chickenExist, sizeof(chickenExist));

    write(fd, &this->m_seedsCount, sizeof(this->m_seedsCount));

    char eggExist = this->m_eggExist;
    write(fd, &eggExist, sizeof(eggExist));
}

void Cell::loadFromFile(int fd)
{
    char chickenExist;
    read(fd, &chickenExist, sizeof(chickenExist));
    this->m_chickenExist = chickenExist;

    read(fd, &this->m_seedsCount, sizeof(this->m_seedsCount));

    char eggExist;
    read(fd, &eggExist, sizeof(eggExist));
    this->m_eggExist = eggExist;
}


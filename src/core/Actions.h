#ifndef ACTIONS_H
#define ACTIONS_H

enum Actions
{
    BACK,
    ADD_SEED,
    GRAB_EGG,
    ADD_HAY,
    GRAB_MILK,
    DO_NOTHING,
    ADD_CELL,
};

#endif

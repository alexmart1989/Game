#include "Application.h"
#define ERROR -1

Application::Application():
    m_game(CELL_COUNT)
{}

int Application::getChoice() const
{
    int choice = 0;
    std::cin >> choice;
    return choice;
}

void Application::setFileName(char* filename) const
{
    std::cout << "Please, write filename: ";
    std::cin >> filename;
    std::cout << "\n";
}

void Application::run()
{
    bool exit = false;
    while(!exit)
    {
        showMainMenu();
        int choice = getChoice();
        switch (choice) {
        case START:
        {
            m_game.start();
            break;
        }
        case SAVE:
        {
            int fd = openFile();
            if (fd != ERROR)
            {
                save(fd);
                close(fd);
                std::cout << "File saved!\n";
            }
            else
            {
                std::cout << "File not saved. Try again\n";
            }
            break;
        }
        case LOAD:
        {
            int fd = openFile();
            if (fd != ERROR)
            {
                m_game = load(fd);
                close(fd);
                m_game.start();
            }
            else
            {
                std::cout << "Game not loaded! Check file name!\n";
            }
            break;
        }
        case EXIT:
        {
            std::cout << "Good buy!\n";
            exit = true;
            break;
        }
        default: std::cout << "Wrong input! Try again!\n";
            break;
        }
    }
}

int Application::openFile() const
{
    char userFilename[BUFFER_MAX];
    setFileName(userFilename);
    int flags = O_RDWR | O_CREAT;
    int modes = S_IRWXU;
    int fd = open(userFilename, flags, modes);
    return fd;
}

void Application::showMainMenu() const
{
    std::cout << "\n****Main Menu****\n\n";
    std::cout << START << " = START\n";
    std::cout << SAVE << " = SAVE\n";
    std::cout << LOAD << " = LOAD\n";
    std::cout << EXIT << " = EXIT\n";
    std::cout << '\n';
}

void Application::save(int fd) const
{
    this->m_game.save(fd);
}

Game Application::load(int fd)
{
    Game tmp;
    tmp.load(fd);
    return tmp;
}

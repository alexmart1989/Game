#ifndef CELLSVECTOR
#define CELLSVECTOR

template <class X>
class CellsVector
{
public:

    CellsVector():
        m_size(0),
        m_cells(nullptr)
    {}

    CellsVector(int cellsCount):
        m_size(cellsCount),
        m_cells(new X[m_size])
    {}

    ~CellsVector(){
        delete[] this->m_cells;
    }

    CellsVector(const CellsVector &obj) {
        this->m_size = obj.m_size;
        this->m_cells = new X[this->m_size];
        for(unsigned int cell = 0; cell < this->m_size; ++cell)
        {
            this->m_cells[cell] = obj.m_cells[cell];
        }
    }

    CellsVector& operator=(CellsVector const &tmp)
    {
        if(this != &tmp)
        {
            delete[] this->m_cells;
            this->m_size = tmp.m_size;
            this->m_cells = new X[this->m_size];
            for(unsigned int cell = 0; cell < this->m_size; ++cell)
            {
                this->m_cells[cell] = tmp.m_cells[cell];
            }
        }
        return *this;
    }

    X *cell(int cellID) const{
        return (&this->m_cells[cellID]);
    }

    int size() const{
        return this->m_size;
    }

    void addCell(){

        int oldArrayCapacity = (this->m_size)++;
        X* currentCells = this->m_cells;
        X* tempCells = new X[this->m_size];
        for(int i = 0; i < oldArrayCapacity; ++i)
        {
            tempCells[i] = currentCells[i];
        }

        delete[] currentCells;
        this->m_cells = tempCells;
    }

    void saveToFile(int fd) const
    {
        write(fd, &this->m_size, sizeof(this->m_size));
        for(unsigned int item = 0; item < this->m_size; ++item)
        {
            this->m_cells[item].saveToFile(fd);
        }
    }

    void loadFromFile(int fd)
    {
        read(fd, &this->m_size, sizeof(this->m_size));
        this->m_cells = new X[this->m_size];
        for(unsigned int item = 0; item < this->m_size; ++item)
        {
            this->m_cells[item].loadFromFile(fd);
        }
    }

private:
    unsigned int m_size;
    X *m_cells;
};

#endif

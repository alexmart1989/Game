#ifndef COWHOUSE_H
#define COWHOUSE_H

class CowHouse {
public:
    CowHouse();
    CowHouse(bool cowExist, int hayStacksCount, bool milkExist);
    CowHouse(bool cowExist, int hayStacksCount, bool milkExist, int daysNotMilked);
    CowHouse(const CowHouse& tmp) = default;
    int hayStacksCount() const;
    void addHayStack();
    bool isMilkExist() const;
    bool isCowExist() const;
    bool grabMilk();
    int getDaysNotMilked() const;
    void tick();
    void saveToFile(int fd) const;
    void loadFromFile(int fd);
private:
    bool m_cowExist;
    unsigned int m_hayStackCount;
    bool m_milkExist;
    unsigned int m_daysNotMilk;
};

#endif

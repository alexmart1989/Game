#include <QApplication>
#include "qt/GameWindow.h"
#include "core/Application.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    GameWindow game;
    game.show();
    int result = app.exec();
    return result;
}





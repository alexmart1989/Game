#include "CellWindow.h"
#include <QVBoxLayout>
#include <QPainter>
#include <QPaintEvent>
#include <QImage>
#include <QFont>

#define MINIMUM_HEIGHT 150
#define MINIMUM_WIDHT 100
#define IMAGE_WIDTH 45
#define IMAGE_HEIGHT 45
#define SIZE_FONT 10
#define START_X 10
#define START_Y 10
#define INCREMENT_Y 10
#define INCREMENT_BEFORE_LABEL 60

CellWindow::CellWindow(Cell chickenCell, QWidget *parent) :
    QPushButton(parent),
    m_chickenCell(chickenCell),
    m_chickenState(":/images/chicken.png"),
    m_eggState(":/images/egg.png")
{
    QVBoxLayout* cellLayout = new QVBoxLayout;
    this->setMinimumSize(QSize(MINIMUM_WIDHT, MINIMUM_HEIGHT));
    this->setLayout(cellLayout);
    repaint();
}

void CellWindow::tryToGrabEgg()
{
    bool isGrabbed = this->m_chickenCell.isEggExist();
    if(isGrabbed)
    {
        this->m_chickenCell.grabEgg();
        emit eggGrabbed();
    }
    repaint();
}

void CellWindow::AddSeeds()
{
    this->m_chickenCell.addSeeds();
    repaint();
}

void CellWindow::onTick()
{
    this->m_chickenCell.tick();
    repaint();
}

void CellWindow::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);
    QFont titleFont("Arial", SIZE_FONT, QFont::Bold);
    QSize sizeImages(IMAGE_WIDTH, IMAGE_HEIGHT);
    QPoint positionItems(START_X, START_Y);
    painter.setFont(titleFont);

    QImage chickenState = m_chickenState.scaled(sizeImages, Qt::KeepAspectRatio);
    positionItems.ry() += INCREMENT_Y;
    if(m_chickenCell.isChickenExist())
    {
        painter.drawImage(positionItems, chickenState);
    }
    else
    {
        m_chickenState.load(":/images/dead.png");
        chickenState = m_chickenState.scaled(sizeImages, Qt::KeepAspectRatio);
        painter.drawImage(positionItems, chickenState);
    }

    positionItems.ry() += INCREMENT_BEFORE_LABEL;
    painter.drawText(positionItems, QString("Seeds: %1").arg(this->m_chickenCell.seedsCount()));

    positionItems.ry() += INCREMENT_Y;
    if(m_chickenCell.isEggExist())
    {
        m_eggState.load(":/images/egg.png");
        QImage eggState = m_eggState.scaled(sizeImages, Qt::AspectRatioMode::KeepAspectRatio);
        painter.drawImage(positionItems, eggState);
    }
    else
    {
        m_eggState.load(":/images/noegg.png");
        QImage eggState = m_eggState.scaled(sizeImages, Qt::AspectRatioMode::KeepAspectRatio);
        painter.drawImage(positionItems, eggState);
    }
}

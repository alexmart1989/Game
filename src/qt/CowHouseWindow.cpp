#include "CowHouseWindow.h"

#include <QVBoxLayout>
#include <QPainter>
#include <QPoint>
#include <QFont>
#include <QSize>

#define MINIMUM_HEIGHT 180
#define MINIMUM_WIDTH 100
#define IMAGE_WIDTH 45
#define IMAGE_HEIGHT 45
#define SIZE_FONT 10
#define START_X 10
#define START_Y 10
#define INCREMENT_Y 10
#define INCREMENT_BEFORE_LABEL 60
#define INCREMENT_BEETWEEN_LABEL 20

CowHouseWindow::CowHouseWindow(CowHouse cowHouse, QPushButton *parent) :
    QPushButton(parent),
    m_cowHouse(cowHouse),
    m_cowState(":/images/cow.png"),
    m_milkState(":/images/nomilk.png")
{
    QVBoxLayout*cowLayout = new QVBoxLayout;
    this->setMinimumSize(QSize(MINIMUM_WIDTH, MINIMUM_HEIGHT));
    this->setLayout(cowLayout);
    repaint();
}

void CowHouseWindow::tryToGrabMilk()
{
    bool isMilked = m_cowHouse.isMilkExist();
    if(isMilked)
    {
        this->m_cowHouse.grabMilk();
        emit cowMilked();
    }
    repaint();
}

void CowHouseWindow::addHayStack()
{
    m_cowHouse.addHayStack();
    repaint();
}

void CowHouseWindow::onTick()
{
    m_cowHouse.tick();
    repaint();
}

void CowHouseWindow::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);
    QFont titleFont("Arial", SIZE_FONT, QFont::Bold);
    QSize sizeImage(IMAGE_WIDTH, IMAGE_HEIGHT);
    QPoint positionItems(START_X, START_Y);
    painter.setFont(titleFont);
    painter.drawText(positionItems, QString("CowHouse"));

    positionItems.ry() += INCREMENT_Y;
    if(m_cowHouse.isCowExist())
    {
        QImage cowState = m_cowState.scaled(sizeImage, Qt::KeepAspectRatio);
        painter.drawImage(positionItems, cowState);
    }
    else
    {
        m_cowState.load(":/images/dead.png");
        QImage cowState = m_cowState.scaled(sizeImage, Qt::KeepAspectRatio);
        painter.drawImage(positionItems, cowState);
    }

    positionItems.ry() += INCREMENT_BEFORE_LABEL;
    painter.drawText(positionItems, QString("HayStack: %1").arg(m_cowHouse.hayStacksCount()));

    positionItems.ry() += INCREMENT_BEETWEEN_LABEL;
    painter.drawText(positionItems, QString("no milked: %1").arg(m_cowHouse.getDaysNotMilked()));

    positionItems.ry() += INCREMENT_Y;
    if(m_cowHouse.isMilkExist())
    {
        m_milkState.load(":/images/milk.png");
        QImage milkNotExist = m_milkState.scaled(sizeImage, Qt::KeepAspectRatio);
        painter.drawImage(positionItems, milkNotExist);
    }
    else
    {
        m_milkState.load(":/images/nomilk.png");
        QImage milkExist = m_milkState.scaled(sizeImage, Qt::KeepAspectRatio);
        painter.drawImage(positionItems, milkExist);
    }
}

﻿#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QSignalMapper>
#include <QTimer>

#include "core/Actions.h"
#include "GameWindow.h"
#include "CellWindow.h"
#include "CowHouseWindow.h"

#define CELLS_COUNT 3
#define DEFAULT_HEIGHT 200
#define DEFAULT_WIDTH 250
#define INTERVAL_TICK 5000

GameWindow::GameWindow(QWidget *parent): QWidget(parent),
    m_cellWindows(new CellWindow*[CELLS_COUNT]),
    m_cowWindow(new CowHouseWindow(CowHouse())),
    m_eggs(0),
    m_milk(0),
    m_eggsCountInfo(new QLabel("no egg")),
    m_milkCountInfo(new QLabel("no milk"))
{
    this->setWindowTitle("FARM");
    setMinimumHeight(DEFAULT_HEIGHT);
    setMinimumWidth(DEFAULT_WIDTH);

    QTimer* timerTick = new QTimer(this);
    timerTick->setInterval(INTERVAL_TICK);
    timerTick->setSingleShot(false);
    connect(timerTick, &QTimer::timeout, this, &GameWindow::onTick);
    timerTick->start();

    QHBoxLayout* mainLayout = new QHBoxLayout;
    this->setLayout(mainLayout);

    QPushButton* feed = new QPushButton("&feed");
    QPushButton* grab = new QPushButton("&grab");
    QPushButton* addHay = new QPushButton("&addHay");
    QPushButton* milk = new QPushButton("&milk");
    QPushButton* rest = new QPushButton("&rest");
    QPushButton* exit = new QPushButton("&EXIT");

    QVBoxLayout* buttonsLayout = new QVBoxLayout;
    buttonsLayout->addWidget(feed);
    buttonsLayout->addWidget(grab);
    buttonsLayout->addWidget(addHay);
    buttonsLayout->addWidget(milk);
    buttonsLayout->addWidget(rest);
    buttonsLayout->addWidget(exit);
    mainLayout->addLayout(buttonsLayout);

    QSignalMapper* buttons = new QSignalMapper(this);
    buttons->setMapping(feed, Actions::ADD_SEED);
    buttons->setMapping(grab, Actions::GRAB_EGG);
    buttons->setMapping(addHay, Actions::ADD_HAY);
    buttons->setMapping(milk, Actions::GRAB_MILK);

    connect(feed, SIGNAL(clicked(bool)), buttons, SLOT(map()));
    connect(grab, SIGNAL(clicked(bool)), buttons, SLOT(map()));
    connect(addHay, SIGNAL(clicked(bool)), buttons, SLOT(map()));
    connect(milk, SIGNAL(clicked(bool)), buttons, SLOT(map()));
    connect(buttons, SIGNAL(mapped(int)), this, SLOT(saveActions(int)));
    connect(rest, &QPushButton::clicked, this, &GameWindow::rest);
    connect(this, &GameWindow::rest, this, &GameWindow::onTick);
    connect(exit, &QPushButton::clicked, this, &GameWindow::close);

    QSignalMapper* cells = new QSignalMapper(this);
    QHBoxLayout* cellsLayout = new QHBoxLayout;
    for(int cellId = 0; cellId < CELLS_COUNT; ++cellId)
    {
        CellWindow *cellWindow = new CellWindow(Cell());
        cellsLayout->addWidget(cellWindow);
        m_cellWindows[cellId] = cellWindow;
        cells->setMapping(cellWindow, cellId);
        connect(cellWindow, &CellWindow::eggGrabbed, this, &GameWindow::onEggGrabbed);
        connect(cellWindow, SIGNAL(clicked(bool)), cells, SLOT(map()));
    }
    connect(cells, SIGNAL(mapped(int)), this, SLOT(selectActionsCell(int)));

    QHBoxLayout* cowLayout = new QHBoxLayout;
    cowLayout->addWidget(m_cowWindow);
    connect(m_cowWindow, &CowHouseWindow::cowMilked, this, &GameWindow::onCowMilked);
    connect(m_cowWindow, &CowHouseWindow::clicked, this, &GameWindow::selectActionsCow);

    QVBoxLayout* animalsLayout = new QVBoxLayout;
    animalsLayout->addLayout(cellsLayout);
    animalsLayout->addLayout(cowLayout);
    mainLayout->addLayout(animalsLayout);

    QVBoxLayout* grabbedLayout = new QVBoxLayout;
    grabbedLayout->addWidget(m_eggsCountInfo);
    grabbedLayout->addWidget(m_milkCountInfo);
    mainLayout->addLayout(grabbedLayout);
}

GameWindow::~GameWindow()
{
    delete[] m_cellWindows;
}

void GameWindow::onEggGrabbed()
{
    m_eggs++;
    updateGrabbedEggs();
}

void GameWindow::onCowMilked()
{
    m_milk++;
    updateGrabbedMilk();
}

void GameWindow::grabEggSelectedCell(int cellId)
{
    m_cellWindows[cellId]->tryToGrabEgg();
}

void GameWindow::grabMilk()
{
    m_cowWindow->tryToGrabMilk();
}

void GameWindow::feedCell(int cellId)
{
    m_cellWindows[cellId]->AddSeeds();
    onTick();
}

void GameWindow::feedCow()
{
    m_cowWindow->addHayStack();
}

void GameWindow::updateGrabbedMilk()
{
    m_milkCountInfo->setText(QString("milk: %1").arg(m_milk));
}

void GameWindow::saveActions(int action)
{
    this->m_action = (Actions)action;
    emit selectedActions(m_action);
}

void GameWindow::selectActionsCell(int cellId)
{
    switch (m_action)
    {
        case Actions::ADD_SEED:
        {
            m_cellWindows[cellId]->AddSeeds();
            break;
        }
        case Actions::GRAB_EGG:
            m_cellWindows[cellId]->tryToGrabEgg();
            break;
    }
}

void GameWindow::selectActionsCow()
{
    switch (m_action)
    {
        case Actions::ADD_HAY:
        {
            m_cowWindow->addHayStack();
            break;
        }
        case Actions::GRAB_MILK:
        {
            m_cowWindow->tryToGrabMilk();
            break;
        }
    }
}

void GameWindow::updateGrabbedEggs()
{
    m_eggsCountInfo->setText(QString("Eggs: %1").arg(m_eggs));
}

void GameWindow::onTick()
{
    for (int cellId = 0; cellId < CELLS_COUNT; ++cellId)
    {
        m_cellWindows[cellId]->onTick();
    }
    m_cowWindow->onTick();
}


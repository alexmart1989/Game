#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>
#include "core/Actions.h"

class CellWindow;
class CowHouseWindow;
class QSignalMapper;
class QVBoxLayout;
class QLabel;

class GameWindow : public QWidget
{
    Q_OBJECT
public:
    GameWindow(QWidget *parent = 0);
    ~GameWindow();
signals:
    void feed(int cellId);
    void addHay();
    void grabEgg(int cellId);
    void milk();
    void rest();
    void selectedActions(int action);
public slots:
    void onEggGrabbed();
    void onCowMilked();
    void onTick();

    void grabEggSelectedCell(int cellId);
    void grabMilk();
    void feedCell(int cellId);
    void feedCow();

    void saveActions(int action);
    void selectActionsCell(int cellId);
    void selectActionsCow();
private:
    void updateGrabbedEggs();
    void updateGrabbedMilk();


    CellWindow** m_cellWindows;
    CowHouseWindow* m_cowWindow;
    int m_eggs;
    int m_milk;
    QLabel* m_eggsCountInfo;
    QLabel* m_milkCountInfo;
    Actions m_action;
};

#endif // GAMEWINDOW_H

#ifndef COWHOUSEWINDOW_H
#define COWHOUSEWINDOW_H

#include <QPushButton>
#include "core/CowHouse.h"

class QPaintEvent;
class QImage;

class CowHouseWindow : public QPushButton
{
    Q_OBJECT
public:
    explicit CowHouseWindow(CowHouse cowHouse, QPushButton *parent = 0);

signals:
    void cowMilked();
public slots:
    void tryToGrabMilk();
    void addHayStack();
    void onTick();

protected:
    virtual void paintEvent(QPaintEvent *event);
private:
    CowHouse m_cowHouse;
    QImage m_cowState;
    QImage m_milkState;
};

#endif // COWHOUSEWINDOW_H

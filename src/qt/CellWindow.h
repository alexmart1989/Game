#ifndef CELLWINDOW_H
#define CELLWINDOW_H

#include <QPushButton>
#include "core/Cell.h"

class QImage;

class CellWindow : public QPushButton
{
    Q_OBJECT
public:
    explicit CellWindow(Cell chickenCell, QWidget *parent = 0);

signals:
    void eggGrabbed();
public slots:
    void tryToGrabEgg();
    void AddSeeds();
    void onTick();
protected:
    virtual void paintEvent(QPaintEvent *event);
private:
    Cell m_chickenCell;
    QImage m_chickenState;
    QImage m_eggState;
};

#endif // CELLWINDOW_H

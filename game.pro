#app, lib, ...

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
TARGET = game
DESTDIR = ./bin
OBJECTS_DIR = ./.obj
CONFIG += c++11
QT += core gui

INCLUDEPATH += src

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
        ./src/core/Game.cpp \
        ./src/core/Cell.cpp \
        ./src/main.cpp \
        ./src/core/CowHouse.cpp \
        ./src/core/Application.cpp \
        ./src/cli/CellTerminal.cpp \
        ./src/cli/CowHouseTerminal.cpp \
        ./src/qt/GameWindow.cpp \
        ./src/qt/CellWindow.cpp \
        ./src/qt/CowHouseWindow.cpp \

HEADERS += \
        ./src/core/Cell.h \
        ./src/core/Game.h \
        ./src/core/CowHouse.h \
        ./src/core/CellsVector.h \
        ./src/core/Application.h \
        ./src/core/ActionsMainMenu.h \
        ./src/core/Actions.h \
        ./src/cli/CellTerminal.h \
        ./src/cli/CowHouseTerminal.h \
        ./src/qt/GameWindow.h \
        ./src/qt/CellWindow.h \
        ./src/qt/CowHouseWindow.h \

RESOURCES += \
    src/pictures.qrc
